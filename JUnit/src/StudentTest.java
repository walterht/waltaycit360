import org.junit.Test;
import static org.junit.Assert.*;

public class StudentTest {
    Student testStudent = new Student("Walter", "Taylor");
    Student nullTest;
    Student arrayTest = new Student("John", "Doe");

    @Test
    public void getFullName() {

        assertEquals("Walter Taylor", testStudent.getFullName());

    }

    @Test
    public void isCurrent() {

        assertTrue(testStudent.isCurrent());
    }

    @Test
    public void deferred() {

        testStudent.deferSemester();
        assertFalse(testStudent.isCurrent());

    }

    @Test
    public void nullStudent() {

        assertNull(nullTest);

    }

    @Test
    public void notNullStudent() {

        nullTest = new Student("Walter", "Taylor", false);
        assertNotNull(nullTest);

    }

    @Test
    public void notSameStudent() {

        assertNotSame(testStudent, nullTest);

    }

    @Test
    public void sameStudent() {

        Student testStudentCopy = testStudent;
        assertSame(testStudent, testStudentCopy);

    }

    @Test
    public void arrayTest() {

        nullTest = new Student("Walter", "Taylor", false);

        String studentArray[] = {testStudent.getFullName(), nullTest.getFullName(), arrayTest.getFullName()};
        String expected[]={"Walter Taylor", "Walter Taylor", "John Doe"};

        assertArrayEquals(expected, studentArray);
    }
}