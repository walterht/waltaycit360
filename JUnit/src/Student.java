public class Student {

    String fName;
    String lName;
    boolean current;

    public Student(String fName, String lName) {
        this.fName = fName;
        this.lName = lName;
        this.current = true;
    }

    public Student() {
        this.fName = null;
        this.lName = null;
    }

    public Student(String fName, String lName, boolean current) {
        this.fName = fName;
        this.lName = lName;
        this.current = current;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    public boolean isCurrent() {
        return current;
    }

    public String getFullName(){
        return (fName + " " + lName);
    }

    public void deferSemester() {
        this.current = false;
    }
}
