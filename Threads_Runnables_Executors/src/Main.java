import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);

        Racer r1 = new Racer("Hare", 25);
        Racer r2 = new Racer("Tortoise", 25);
        Racer r3 = new Racer("Falcon", 25);
        Racer r4 = new Racer("Dolphin", 25);
        Racer r5 = new Racer("Panther", 25);

        myService.execute(r1);
        myService.execute(r2);
        myService.execute(r3);
        myService.execute(r4);
        myService.execute(r5);

        myService.shutdown();

    }

}
