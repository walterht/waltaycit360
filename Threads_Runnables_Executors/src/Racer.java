import java.util.Random;

public class Racer implements Runnable{

    String name;
    int counter;
    int sleeping;

    public Racer(String name, int counter) {
        this.name = name;
        this.counter = counter;
        Random random = new Random();
        sleeping = random.nextInt(1000, 10001);
    }

    @Override
    public void run() {

        System.out.println(name + " is running for " + counter + " and will sleep for " + sleeping + "\n");

        for(int i = counter; i > 0; i--){
            System.out.print(name + " at " + i + "  |  ");
            //Comment out the following try catch for second demonstration.
            /*try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            if(i%7 == 0){
                try {
                    System.out.print(name + " sleeping  |  ");
                    Thread.sleep(sleeping);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("\n" +name + " FINISHED\n");
    }
}
