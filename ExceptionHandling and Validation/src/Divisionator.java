import java.util.Scanner;

public class Divisionator {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        double num=0, den=0;
        double answer = 0;
        boolean loop = true;
        boolean failed = true;
        String input = null;

        /*
        This section showcases Exception Handling with the program to divide two numbers from user input.
         */
        System.out.println("Welcome to the Divisionator. First we will divide two numbers and use Exception Handling.\n");

        while (loop){
            try {
                System.out.println("Please enter the numerator for the division:");
                input = sc.nextLine();
                num = Double.parseDouble(input);
                loop = false;
                failed = false;
            }
            catch (NumberFormatException error){
                System.out.println("ERROR! - You entered: '" + input + "' which is not a number.");
            }
            finally {
                if(failed){
                    System.out.println("Please only input Number Values and a Decimal Point when applicable.\n");
                }
                else{
                    System.out.println("Numerator saved.\n");
                }
            }
        }

        //Set loop and failed variables to true again for denominator input loop
        loop = true;
        failed = true;

        while(loop){
            try{
                System.out.println("Please enter the denominator for the division:");
                input = sc.nextLine();
                den = Double.parseDouble(input);
                answer = excDivision(num, den);
                loop = false;
                failed = false;
            }
            catch (NumberFormatException error){
                System.out.println("ERROR! - You entered: '" + input + "' which is not a number.");
            }
            catch (ArithmeticException error2){
                System.out.println("ERROR! - You entered a 0 for the denominator. The denominator can not be a 0.");
            }
            finally {
                if(failed){
                    System.out.println("Please only input Number Values and a Decimal Point when applicable.\n");
                }
                else{
                    System.out.println("Denominator saved. Dividing...");
                }
            }
        }

        System.out.println("The result is " + answer);

        /*
        This section showcases Data Validation on the user input received to divide two numbers.
         */
        System.out.println("\nCongratulations! You used the Divisionator through the Exception Handling Experience");
        System.out.println("Now, you can experience the Data Validation version of the Divisionator!");

        loop = true;
        Validator validInput = new Validator();

        while (loop){
            System.out.println("Please enter the numerator for the division:");
            input = sc.nextLine();
            validInput.setInput(input.trim());
            if(!validInput.isValid() || input.isEmpty()){
                System.out.println("ERROR! - You entered: '" + input + "' which is not a number.");
                System.out.println("Please only input Number Values and a Decimal Point when applicable.\n");
            }
            else{
                num = Double.parseDouble(input);
                System.out.println("Numerator saved.\n");
                loop = false;
            }
        }

        loop = true;
        while (loop){
            System.out.println("Please enter the denominator for the division:");
            input = sc.nextLine();
            validInput.setInput(input.trim());
            if(!validInput.isValid()){
                System.out.println("ERROR! - You entered: '" + input + "' which is not a number.");
                System.out.println("Please only input Number Values and a Decimal Point when applicable.\n");
            }
            else if(input.equals("0")){
                System.out.println("ERROR! - You entered: 0. The denominator can not be a 0.");
            }
            else{
                den = Double.parseDouble(input);
                System.out.println("Denominator saved. Dividing...");
                loop = false;
            }
        }

        answer = valDivision(num, den);

        System.out.println("The result is " + answer);

    }

    static double excDivision(double num, double den) throws ArithmeticException{

        if(den == 0){
            throw new ArithmeticException();
        }

        return num / den;
    }

    static double valDivision(double num, double den){
        return num / den;
    }
}
