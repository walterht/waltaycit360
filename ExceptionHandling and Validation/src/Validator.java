//This class validates that the input given
public class Validator {

    String input;
    boolean valid;

    public Validator() {
        this.input = null;
        this.valid = true;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public boolean isValid(){

        for(int i =0;i<this.input.length();i++){
            if(!Character.isDigit(this.input.charAt(i)) && !(this.input.charAt(i) == '.')){
                this.valid = false;
                break;
            }
        }
        boolean result = this.valid;
        if(!this.valid){
            this.valid = true;
        }

        return result;
    }
}
