import java.util.*;

public class Student {

    String name;
    String topic;
    Integer weekTaught;

    public Student(String name, String topic, Integer weekTaught) {
        this.name = name;
        this.topic = topic;
        this.weekTaught = weekTaught;
    }

    @Override
    public String toString() {
        return "Week " + weekTaught + ": " + topic + " by " + name;
    }
}

class studentComparator implements Comparator<Student>{

    @Override
    public int compare(Student s1, Student s2){

        if(s1.weekTaught < s2.weekTaught){
            return -1;
        }
        else if(s1.weekTaught > s2.weekTaught){
            return 1;
        }
        else return 0;

    }

}
