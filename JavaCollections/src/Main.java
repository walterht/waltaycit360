import java.util.*;

public class Main {
    public static void main(String[] args) {

    /*
        List example
    */
        List<String> studentList = new ArrayList<>();

        studentList.add("Steven");
        studentList.add("Richard");
        studentList.add("Dustin");
        studentList.add("Mark");
        studentList.add("Dainon");
        studentList.add("Marvel");
        studentList.add("Walter");

        System.out.println("Group 4 students' are:");
        for (String student : studentList) {
            System.out.println(student);
        }

    /*
    Set example
     */
        Set<String> studentSet = new HashSet<>();

        studentSet.add("Steven");
        studentSet.add("Richard");
        studentSet.add("Dustin");
        studentSet.add("Mark");
        studentSet.add("Dainon");
        studentSet.add("Mark");
        studentSet.add("Marvel");
        studentSet.add("Walter");

        System.out.println("Now in a HashSet. Notice that although Mark was input twice, he will only appear once.\n" +
                "And also, that they will be printed out in different order.\n");
        for (String student : studentSet) {
            System.out.println(student);
        }

    /*
    Queue Example
     */
        Queue<Student> studentQueue = new PriorityQueue<>(10, new studentComparator());

        studentQueue.add(new Student("Walter", "Java Collections", 5));
        studentQueue.add(new Student("Steven", "Servlets", 7));
        studentQueue.add(new Student("Dustin", "Data Validation", 3));
        studentQueue.add(new Student("Marvel", "Exception Handling", 3));
        studentQueue.add(new Student("Dainon", "Use Case Documents", 6));

        System.out.println("Who teaches first?");
        System.out.println(studentQueue.peek());

        for (int i = 0; i < 4; i++) {
            try {
                System.out.println(studentQueue.remove());
            } catch (Exception e) {
                System.out.println("There's nothing left in the queue");
            }
        }

        System.out.println("Who's left to teach?");
        System.out.println(studentQueue.peek());

        studentQueue.add(new Student("Walter", "Threads, Runnables, Executors", 8));
        studentQueue.add(new Student("Mark", "Servlets", 7));
        studentQueue.add(new Student("Dainon", "Hibernate", 9));

        while (!studentQueue.isEmpty()) {
            try {
                System.out.println(studentQueue.remove());
            } catch (Exception e) {
                System.out.println("There's nothing left in the queue");
            }
        }

    /*
    Tree Example
     */
        Set<String> studentTree = new TreeSet<>();
        studentTree.add("Steven");
        studentTree.add("Walter");
        studentTree.add("Richard");
        studentTree.add("Marvel");
        studentTree.add("Dustin");
        studentTree.add("Mark");
        studentTree.add("Dainon");

        System.out.println("In a TreeSet, items are ordered. However, they still cannot have duplicates.");
        for (String student : studentTree) {
            System.out.println(student);
        }

    }
}