import entity.ClientEntity;

import javax.persistence.*;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();


        try {
            transaction.begin();
            //Creates instance HibernateUtils class to call its methods.
            HibernateUtils hUtils = new HibernateUtils();
            //Queries the client table and displays beginning set of data already on table
            List<ClientEntity> cList = hUtils.getClients(em);
            System.out.println("Original set of clients:");
            cList.forEach(System.out::println);
            //Creates new instance of ClientEntity to insert into db
            ClientEntity client = new ClientEntity();
            client.setFirstName("John");
            client.setLastName("Doe");
            client.setEmail("john@email.com");
            //Calls method to insert new row to db
            hUtils.insertClient(em,transaction,client);
            //Selects all data from clients table and displays them to show new data
            cList = hUtils.getClients(em);
            System.out.println("\nNew set of clients:");
            cList.forEach(System.out::println);
            //Selects a specific client by ID
            System.out.println("\nClient with ID 2:");
            System.out.println(hUtils.getClientById(em, 2));
        }
        catch (PersistenceException e){
            System.err.println(e.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if(transaction.isActive()){
                transaction.rollback();
            }
            em.close();
            emf.close();
        }

    }

}
