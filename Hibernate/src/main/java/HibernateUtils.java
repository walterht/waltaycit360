import entity.ClientEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.List;

public class HibernateUtils {

    public void insertClient(EntityManager em, EntityTransaction transaction, ClientEntity client)throws PersistenceException {

        try {
            em.persist(client);
            transaction.commit();
            System.out.println("\nInsert of " + client.getFirstName() + " " + client.getLastName() + "succesful.");
        }
        catch (PersistenceException e){
            throw e;
        }

    }

    public List<ClientEntity> getClients(EntityManager em) throws Exception{
        try{
            String query = "FROM ClientEntity";
            return em.createQuery(query, ClientEntity.class).getResultList();
        }
        catch (Exception e){
            throw e;
        }
    }

    public ClientEntity getClientById(EntityManager em, int id) throws Exception{
        try{
            String query = "FROM ClientEntity WHERE clientId=" + id;
            return em.createQuery(query, ClientEntity.class).getSingleResult();
        }
        catch (Exception e){
            throw e;
        }
    }

}
