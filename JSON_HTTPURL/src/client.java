import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class client {

    public static void main(String[] args) {

        System.out.println("Connecting to localhost:8400");
        String movieJSON = getHttpData("http://localhost:8400");
        System.out.println("This is the JSON string obtained:");
        System.out.println(movieJSON);

        movie movie = jsonToMovie(movieJSON);
        System.out.println("\nThis is the Object obtained after parsing the JSON string to the object:");
        System.out.println(movie);


    }

    //Connects to the server and gets the content from it.
    public static String getHttpData(String link){

        try{
            URL url = new URL(link);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String line = null;

            while ((line = reader.readLine())!=null){
                sb.append(line + "\n");
            }

            return sb.toString();
        }
        catch (IOException e){
            System.err.println(e.toString());
        }

        return "Error";
    }

    //Converts JSON string to movie object
    public static movie jsonToMovie(String movJson){

        ObjectMapper mapper = new ObjectMapper();
        movie movie = null;

        try{
            movie = mapper.readValue(movJson, movie.class);
        }
        catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return movie;
    }

}
