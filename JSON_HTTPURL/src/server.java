import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class server {

    public static void main(String[] args) throws IOException {

        HttpServer server = HttpServer.create(new InetSocketAddress("localhost",8400), 0);
        server.createContext("/", new MyHandler());
        server.setExecutor(null);
        server.start();

    }

    static class MyHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException{

            movie spiderMan = new movie("Spider-Man: No Way Home", 2021, 8.7);
            String response = movieToJSON(spiderMan);

            exchange.getResponseHeaders().set("Content-Type", "/");
            exchange.sendResponseHeaders(200, response.getBytes(StandardCharsets.UTF_8).length);
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

    }

    public static String movieToJSON(movie mov){

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try{
            s = mapper.writeValueAsString(mov);
        }
        catch (JsonProcessingException e){
            System.err.println(e.toString());
        }

        return s;
    }

}
